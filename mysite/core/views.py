from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView, CreateView
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy
from django.core.mail import send_mail
from .forms import FileForm
from .models import File
from django.conf import settings

import os
import shutil


class Home(TemplateView):
    template_name = 'home.html'


def upload(request):
    context = {}
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        context['url'] = fs.url(name)
    return render(request, 'upload.html', context)


def file_list(request):
    files = File.objects.all()
    return render(request, 'file_list.html', {
        'files': files
    })


def upload_file(request):
    if request.method == 'POST':
        form = FileForm(request.POST, request.FILES)

        # rename file
        ss = request.FILES['file'].name
        request.FILES['file'].name = request.POST['name_of_file'] + '.' + ss[ss.rfind('.')+1:]

        # rename cover
        img_name = request.FILES['cover'].name
        request.FILES['cover'].name = request.POST['name_of_file'] + '.' + img_name[img_name.rfind('.')+1:]

        if form.is_valid():
            form.save()

            # create txt
            with open(settings.MEDIA_ROOT +'/files/' + request.POST['name_of_file'] + '.txt', 'w+') as f:
                f.write(request.POST['info'])
            # send email
            send_mail(
                'New file recived.',
                'Please check file',
                'erjigit17@gmail.com',
                ['erjigit17@gmail.com'],
                fail_silently=False,
            )

            return redirect('file_list')

    else:
        form = FileForm()
    return render(request, 'upload_file.html', {
        'form': form
    })


def delete_file(request, pk):
    if request.method == 'POST':
        file = File.objects.get(pk=pk)
        file.delete()

        # delete txt
        link = settings.MEDIA_ROOT + '/files/' + str(file) + '.txt'
        if os.path.exists(link):
            os.remove(link)


    return redirect('file_list')


def approved_file(request, pk):
    # if request.method == 'POST':
    file = File.objects.get(pk=pk)
    file.move_cover()
    file.move_file()

    # copy txt to pub
    src = settings.MEDIA_ROOT + '/files/' + str(file) + '.txt'
    dst = settings.MEDIA_ROOT + '/pub/' + str(file) + '.txt'
    shutil.move(src, dst)

    file.delete()
    return redirect('file_list')



class FileListView(ListView):
    model = File
    template_name = 'class_file_list.html'
    context_object_name = 'files'


class UploadFileView(CreateView):
    model = File
    form_class = FileForm
    success_url = reverse_lazy('class_file_list')
    template_name = 'upload_file.html'
