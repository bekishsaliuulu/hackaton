from django.urls import reverse_lazy
from django.views.generic import CreateView

from .forms import CustomUserCreationForm
from django.contrib.auth.decorators import login_required


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


# Bekish
def login(request):
    return render(request, 'login.html')


@login_required
def home(request):
    return render(request, 'home.html')
# Bekish